# Hashicorp Vault

Secret Sprawl is the state of the world where secrete are being generate everywhere and are super hard to track. 

This is a problem and a security risk.

Sometime they exist in files next to the code, inside software like Jenkins, in user computer and so on. 


## Agenda 

- Problems & Good practices
- Installing vault
- Main commands & Secretes


### Problems

- Hard to organise
- Hard to track who is ususing what
- If keys/secretes are used in multiple location then you compromise several servers if you loose one key
- Generally this is a pain for organisations
- Kept with out encryption


### Good practices

- Principle of least privilidge - you give the mininum amount security clearance needed
- Time stamp / time to live the keys - Keys/secrete time out - Creates a moving target for bad actors
- individual access for machines or people


INTRODUCING: VAULT from hashicorp

## Installing Vault

https://learn.hashicorp.com/tutorials/vault/getting-started-install?in=vault/getting-started

```bash
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

sudo apt-get update && sudo apt-get install vault

```

Then start your vault dev server, keep the keys & tokens and export the VAULT_ADDR.

```bash 

$ vault server -dev

```

Start a new terminal and copy the keys and tokens to a file.

Export

```bash

$ export VAULT_ADDR='http://127.0.0.1:8200'

```

### Main commands 


```bash 
#name spaces to create secretes
$ vault secrets list

## Creating secrete with Key value paird

$ vault kv put secret/api_key key=1883i3jdikeirn

$ vault kv put secret/filipe_scrt token=helloLondon

## Get the secrets 

$ vault kv get secret/api_key

$ vault kv get secret/filipe_scrt

## Get the Json file

$ vault kv get -format=json secret/api_key

$ vault kv get -format=json secret/api_key | jq

vault kv get -format=json secret/api_key | jq .data.data.key

## delete one key valau pair

vault kv delete secret/filipe_scrt

```

### Secret Engines

Vault allows you to store different types of keys and uses difirent types of engines.

We just used Key-Value - `vault kv`

If we try and use `kv` in a location other than `secret/(...)` it breaks. 

```bash

$ vault kv put secret_filipe_folder/del_me key=lotsofpoun

> (...)
> Code: 403. Errors:
> (...)

```

You can create new locations for secrets and also you can use different types of engines: 

- kv - key value store
- AWS - AWS IAM access keys and credentials 
- databases - timelime creditials with rotation
- identety keys
- others 

```bash 
# check existing vault location & secret engines using

$ vault secrets list

```


Create your own location with specific vault engines using:


```bash
## lets create a kv_more vault location

$ vault secrets enable -path=kv_more kv

$ vault kv put kv_more/new_key key=378uhuhujhfbjdn

$ vault kv put kv_more/mega_secrete key=378uhuhujhfbjdn

```

Now we might need to deactivate / disable an entire vault location 


## Dynamic Secrets

This allows you to create secrets as you need them!

They do not exist prior to you needing them- so nothing to be stolen! 

And then can be revioke as soon as you are done or on a TTL certificate. So AMAZING security. 

We're going to do this for aws. 

```bash 

# create a place for aws secrets with aws secrets engine

$ vault secrets enable -path=aws_access aws

> Path           Type
> ----           ----
> aws_access/    aws
> (...)

```

We now need to give vault authorization & authenthication to interact with AWS before we can go create dynamic keys. 

There are two method os authenthication: 

1. set them as environment variables and then interpolate (not for production)
2. set a machine role with the IAM policies (better - no keys exchanged)

** Environment Variables and interpolate **

```bash
export AWS_ACCESS_KEY_ID=<aws_access_key_id>
export AWS_SECRET_ACCESS_KEY=<aws_secret_key>

vault write aws_access/config/root \
    access_key=$AWS_ACCESS_KEY_ID \
    secret_key=$AWS_SECRET_ACCESS_KEY \
    region=eu-west-1

```

Now vault can create keys. 

However, they keys we create need to follow certain roles. 

Let's create a role for keys to be create with.

```bash

vault write aws_access/roles/my-role \
        credential_type=iam_user \
        policy_document=-<<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1426528957000",
      "Effect": "Allow",
      "Action": [
        "ec2:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF

```


Then we need to read this role to make generate keys:

```bash
$ vault read aws_access/creds/my-role
```

Then you can revoke:

```
$ vault lease revoke aws_access/creds/my-role/ymjbdlpWdQ3vb2Kot3QkBOrS
```